package com.example.figures.ui

import android.content.Context
import android.graphics.*
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.Shape
import android.view.MotionEvent
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.figures.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlin.random.Random

class PaintView(context: Context): View(context) {

    private lateinit var extraCanvas: Canvas
    private lateinit var extraBitmap: Bitmap

    private var motionTouchEventX = 0f
    private var motionTouchEventY = 0f

    private var currentX = 0f
    private var currentY = 0f

    private var objects: ArrayList<Pair<RectF, Int>> = arrayListOf()
    private val colors = listOf(Color.CYAN, Color.GREEN, Color.RED, Color.MAGENTA)

    private var dragObjectIndex = -1

    // Set up the paint with which to draw.
    private val paint = Paint().apply {
        color = Color.BLACK
        // Smooths out edges of what is drawn without affecting shape.
        isAntiAlias = true
        // Dithering affects how colors with higher-precision than the device are down-sampled.
        isDither = true
        strokeJoin = Paint.Join.ROUND // default: MITER
        strokeCap = Paint.Cap.ROUND // default: BUTT
        strokeWidth = 12f // default: Hairline-width (really thin)
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        if (::extraBitmap.isInitialized) extraBitmap.recycle()
        extraBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        extraCanvas = Canvas(extraBitmap)
        extraCanvas.drawColor(Color.BLUE)

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        extraBitmap.eraseColor(Color.BLUE)
        for (obj in objects) {
            paint.setColor(obj.second)
            extraCanvas.drawOval(obj.first, paint)
        }
        canvas.drawBitmap(extraBitmap, 0f, 0f, null)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        motionTouchEventX = event.x
        motionTouchEventY = event.y

        println("debug: onTouchEvent $motionTouchEventX $motionTouchEventY")
        when (event.action) {
            MotionEvent.ACTION_DOWN -> touchStart()
            MotionEvent.ACTION_MOVE -> touchMove()
            MotionEvent.ACTION_UP -> touchUp()
        }
        return true
    }



    private fun touchStart() {
        currentX = motionTouchEventX
        currentY = motionTouchEventY
        println("debug: touchStart $currentX $currentY")
    }

    private fun touchUp() {
        println("debug: in touchUp")
        dragObjectIndex = -1
        if (motionTouchEventY != currentY || motionTouchEventX != currentX)
            return
        val radius = 100
        val oval = RectF(
            motionTouchEventX-radius,
            motionTouchEventY-radius,
            motionTouchEventX+radius,
            motionTouchEventY+radius)

        objects.add( oval to colors[Random.nextInt(0, colors.size-1)])
        println("debug: added! ${objects.size}")
        invalidate()
    }

    private fun touchMove() {
        println("debug: touchMove")
        val dx = motionTouchEventX - currentX
        val dy = motionTouchEventY - currentY

        if (dragObjectIndex == -1) {
            val ind = findIntersection(motionTouchEventX, motionTouchEventY)
            if (ind == -1) return
            dragObjectIndex = ind
        }
        val ind = dragObjectIndex
        println("debug: binded on $ind")
        val radiusX = (objects[ind].first.right - objects[ind].first.left)/2
        val radiusY = (objects[ind].first.bottom - objects[ind].first.top)/2

        objects[ind].first.left = currentX - radiusX
        objects[ind].first.right = currentX + radiusX
        objects[ind].first.top = currentY - radiusY
        objects[ind].first.bottom = currentY + radiusY

        currentX = motionTouchEventX
        currentY = motionTouchEventY

        invalidate()
    }

    private fun findIntersection(x: Float, y: Float): Int {
        for (i in 0 until objects.size) {
            val oval = objects[i].first
            if (x in oval.left..oval.right && y in oval.top..oval.bottom)
                return i
        }
        return -1
    }

    fun clear() {
        extraBitmap.eraseColor(Color.BLUE)
    }
}