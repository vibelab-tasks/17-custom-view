package com.example.figures

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import com.example.figures.ui.PaintView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val paintView = PaintView(this)
        // No XML file; just one custom view created programmatically.
        // Request the full available screen for layout.
        paintView.systemUiVisibility = SYSTEM_UI_FLAG_FULLSCREEN
        paintView.contentDescription = "a"
        setContentView(paintView)
    }

}